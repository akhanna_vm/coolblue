import pandas as pd
import numpy as np
from sklearn.feature_extraction.text import HashingVectorizer
from sklearn.cluster import MeanShift, estimate_bandwidth
import matplotlib.pyplot as plt
from sklearn import decomposition
def cluster(folder, date,filename):
	path = folder+date+"/"+filename
	df = pd.read_csv(path, sep="\t", names=['session','product'], header=1)
	print(len(df))
	vectorizer = HashingVectorizer(tokenizer=lambda doc: doc, lowercase=False,n_features=1000, ngram_range = (2, 2))
	X = vectorizer.transform(df['product'].values)

	pca = decomposition.PCA(n_components=2)
	pca.fit(X.toarray())
	X = pca.transform(X.toarray())

	bandwidth = estimate_bandwidth(X, quantile=0.2, n_samples=500)
	kmeans = MeanShift(bandwidth=bandwidth, bin_seeding=True)
	kmeans.fit(X)

	plt.figure(1)
	plt.clf()
	x_min, x_max = X[:, 0].min() + 1, X[:, 0].max() - 1
	y_min, y_max = X[:, 1].min() + 1, X[:, 1].max() - 1
	plt.plot(X[:, 0], X[:, 1], 'k.', markersize=2)
	# Plot the centroids as a white X
	centroids = kmeans.cluster_centers_
	plt.scatter(centroids[:, 0], centroids[:, 1],
		    marker='x', s=169, linewidths=3,
		    color='r', zorder=10)
	plt.title('Means Shift clustering-products visited in a session.')
	plt.xticks(())
	plt.yticks(())
	#plt.show()
	plt.savefig(date+"-mean-shift"+".png")

cluster("../results/","2013-06-19","part-00000-00004")
cluster("../results/","2013-06-20","part-00000-00000")
cluster("../results/","2013-06-21","part-00000-00005")
cluster("../results/","2013-06-22","part-00000-00003")
cluster("../results/","2013-06-23","part-00000-00001")
cluster("../results/","2013-06-24","part-00000-00002")
cluster("../results/","2013-06-25","part-00000-00006")
cluster("../results/","2013-06-26","part-00000-00007")


