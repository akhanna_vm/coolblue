package nl.coolblue.flow;

import java.util.Properties;

import nl.coolblue.aggregator.ProductId;
import nl.coolblue.function.CleanVisit;
import cascading.flow.Flow;
import cascading.flow.FlowConnector;
import cascading.flow.FlowDef;
import cascading.flow.hadoop.HadoopFlowConnector;
import cascading.pipe.Each;
import cascading.pipe.Every;
import cascading.pipe.GroupBy;
import cascading.pipe.Pipe;
import cascading.property.AppProps;
import cascading.scheme.Scheme;
import cascading.scheme.hadoop.TextDelimited;
import cascading.scheme.hadoop.TextLine;
import cascading.tap.SinkMode;
import cascading.tap.Tap;
import cascading.tap.hadoop.GlobHfs;
import cascading.tap.hadoop.Hfs;
import cascading.tap.hadoop.PartitionTap;
import cascading.tap.partition.DelimitedPartition;
import cascading.tuple.Fields;

/*
 * CleanVisitFlow, creates a flow which clean the raw data and return only important information.
 * 
 * */
public class SessionProductsFlow {
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static Flow createSessionProductsFlow(String inputFolder, String outputFolder) {
		// Input Tap
		Fields sourceField = new Fields("visit");
		Scheme sourceScheme = new TextLine(sourceField);
		Tap source = new GlobHfs(sourceScheme, inputFolder);

		// Output Tap
		Fields sinkField = new Fields("session", "products");
		Scheme sinkScheme = new TextDelimited(sinkField);
		
		// Delimiters on Date
		DelimitedPartition partition = new DelimitedPartition(new Fields("date"));
		Hfs sink = new Hfs(sinkScheme, outputFolder, SinkMode.REPLACE);
		Tap dayTap = new PartitionTap(sink, partition, SinkMode.REPLACE);

		// Trap Tap
		Scheme sourceTrapScheme = new TextLine(sourceField);
		Tap trap = new Hfs(sourceTrapScheme, outputFolder + "TRAP", SinkMode.REPLACE);

		//Create Pipe
		Pipe assembly = new Pipe("cleaning visit");
		
		// Cleaning Data
		CleanVisit cleanVisit = new CleanVisit(new Fields("session", "product", "date"));
		assembly = new Each(assembly, cleanVisit);

		// Grouping Data by session
		assembly = new GroupBy(assembly, new Fields("session", "date"));
		
		// Producing products for each session
		assembly = new Every(assembly, new ProductId(new Fields("products")));

		Properties properties = new Properties();
		AppProps.setApplicationJarClass(properties, SessionProductsFlow.class);

		// Flow Definition
		FlowDef flowDef = FlowDef.flowDef();
		flowDef.setName("cleanSession").addSource(assembly, source).addTailSink(assembly, dayTap).addTrap(assembly, trap);

		// Flow
		FlowConnector flowConnector = new HadoopFlowConnector(properties);
		Flow flow = flowConnector.connect(flowDef);
		return flow;
	}
}
