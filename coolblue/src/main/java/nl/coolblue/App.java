package nl.coolblue;

import cascading.flow.Flow;
import nl.coolblue.flow.SessionProductsFlow;


public class App {
	public static void main( String[] args )
    {
    	String inputFolder="resources/";
    	String outputFolder="results/";
    	Flow cleanVisitInfo = SessionProductsFlow.createSessionProductsFlow(inputFolder, outputFolder);
    	cleanVisitInfo.complete();
    }
}
