package nl.coolblue.function;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import nl.coolblue.utils.TrapException;
import cascading.flow.AssemblyPlanner.Context;
import cascading.flow.FlowProcess;
import cascading.operation.BaseOperation;
import cascading.operation.Function;
import cascading.operation.FunctionCall;
import cascading.tuple.Fields;
import cascading.tuple.Tuple;
import cascading.tuple.TupleEntry;

/*
 * CleanVisit, cleans the raw input tuple, delimited by '|'.
 * Provides valid tuple and Trap tuples which are not valid.
 * */
public class CleanVisit extends BaseOperation<Context> implements Function<Context> {
	private List<String> botUserAgents;

	public CleanVisit(Fields fieldDeclaration) {
		super(1, fieldDeclaration);
		this.botUserAgents = new ArrayList<String>();
		// Adding all the botUserAgent known.
		this.botUserAgents.add("http://www.bing.com/bingbot.htm");
		this.botUserAgents.add("http://www.google.com/bot.html");
		this.botUserAgents.add("http://www.google.com/adsbot.html");
		this.botUserAgents.add("http://help.yahoo.com/help/us/ysearch/slurp");
		this.botUserAgents.add(" http://help.yahoo.com/help/us/ysearch/slurp");
	}

	/**
	 * Adds tuple which are valid.
	 * There should be at-least 7 parameters.
	 * Should not be a bot.
	 * Session should be valid, not \N, blank or null.
	 * Status code should be 200.
	 * Product Id should be found in the url.
	 * Date format should be MM/dd/yyyy hh:mm:ss a or MM/dd/yyyy.
	 * This method always returns immediately, if the above conditions are not
	 * met.
	 * 
	 * @param flowProcess
	 * @param fucntionCall
	 * @add add valid tuple as provided in Constructor.
	 */
	public void operate(FlowProcess flowProcess, FunctionCall<Context> functionCall) throws TrapException {
		TupleEntry arguments = functionCall.getArguments();

		String visit = arguments.getString("visit");
		String[] visitPrameter = visit.split("\\|");
		int length = visitPrameter.length;
		if (length < 7) {
			throw new TrapException(); // Remove tuple if there are less
										// parameters.
		}

		boolean bot = Boolean.getBoolean(visitPrameter[length - 1]);
		if (bot) {
			return; // Remove tuple if its a bot and throw exception
		}
		String session = visitPrameter[length - 2];

		if (session.trim().length() == 0 || session.trim().equals("\\N")) {
			return; // Remove tuple if session is not specified.
		}

		String userAgent = visitPrameter[length - 3];
		if (isBot(userAgent)) {
			return; // Remove tuple if user agent specify that its a bot.
		}

		if (!isStatusCodeValid(visitPrameter[length - 5])) {
			return; // Remove tuple if status code is not 200-OK.
		}
		
		// Finding product Id.
		int product = getProductId(visitPrameter);
		if (product == 0) {
			throw new TrapException(); // Remove tuple if product id is not
										// found.
		}
		// Parse dt into date format and get date
		String strDate = getDate(visitPrameter[0]);

		// Tuple creation
		Tuple cleanDescription = new Tuple();
		cleanDescription.add(session);
		cleanDescription.add(product);
		cleanDescription.add(strDate);
		functionCall.getOutputCollector().add(cleanDescription);
	}

	/**
	 * Method checks if date is in correct format and returns it.
	 * 
	 * @param dateTime
	 *            .
	 * @return date in yyyy-MM-dd.
	 */
	private String getDate(String dateTime) throws TrapException {
		String strDate = "";
		Date date = null;
		SimpleDateFormat dayFormat = new SimpleDateFormat("yyyy-MM-dd");
		try {
			SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss a");
			date = dateFormat.parse(dateTime);
			strDate = dayFormat.format(date);
		} catch (ParseException e) {
			try {
				SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
				date = dateFormat.parse(dateTime);
				strDate = dayFormat.format(date);
			} catch (ParseException ex) {
				throw new TrapException();
			}
		}
		return strDate;
	}

	/**
	 * Method checks if product id exists, and is valid.
	 * 
	 * @param visitPrameter
	 *            : Array containing all the parameters.
	 * @return product id.
	 */
	private int getProductId(String[] visitPrameter) throws TrapException {
		for (int i = 1; i < visitPrameter.length - 5; i++) {
			String productUrl = visitPrameter[i];
			Pattern p2 = Pattern.compile("product/(\\d+)");
			Matcher m2 = p2.matcher(productUrl);
			if (m2.find()) {
				try {
					int product = Integer.valueOf(m2.group(1));
					return product;
				} catch (NumberFormatException ex) {
					throw new TrapException();
				}
			}
		}
		return 0;
	}

	/**
	 * Method checks for the correct status code.
	 * 
	 * @param strStatusCode
	 *            : Specifying the status code.
	 * @return true it status code is 200, else false.
	 */
	private boolean isStatusCodeValid(String strStatusCode) throws TrapException {
		int statusCode = 0;
		try {
			statusCode = Integer.valueOf(strStatusCode);
			if (statusCode == 200) {
				return true; // Remove tuple if status code is not 200-OK
			}
		} catch (NumberFormatException ex) {
			throw new TrapException();
		}
		return false;
	}

	/**
	 * Method returns if the userAgent specify if its a bot or not.
	 * 
	 * @param userAgent
	 *            : String specifying the user agent
	 * @return true it specifies a bot, false otherwise.
	 */
	private boolean isBot(String userAgent) {
		for (String botUserAgent : this.botUserAgents) {
			if (userAgent.contains(botUserAgent)) {
				return true;
			}
		}
		return false;
	}

}
