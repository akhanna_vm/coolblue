Task to see if there is any relationship between the products visited during a session.


* Visited pages, relationship between a session.

# Map-Reduce #
* SessionProductsFlow.java: creates a flow used to clean all the data from the input resource. (src/main/java/nl/coolblue/flow/)

#INPUT# (/resources)
*  Dt			 Datetimestamp
* request			The requested productpage
* status_code		Http status code
* response_length	        The number of bytes
* user_agent		http://en.wikipedia.org/wiki/User_agent
* php_session		The unique session of a visitor. It stays valid until the visitor closes it’s browser.
* is_bot			When ‘true’ we know for sure that the request has been done by a bot.


#OUTPUT# (/results)
Date separated.

 * Session                  SessionId
 * Products                 ',' separated products.




#Analysis #
* /ml folder:
* analysis.py: Hashing vectorization with n_gram=2 and Mean shift clustering.
* Generates Clustering for sessions, on the output files of map reduce (separated with date).
* Generated png images of cluster formed.